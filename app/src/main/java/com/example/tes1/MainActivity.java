package com.example.tes1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    int angka = 0;

    @Override
    public void onBackPressed() {
        Intent i = new Intent(MainActivity.this,NavigationActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.btnAngka);
        Button reset = findViewById(R.id.btnReset);
        final TextView txtAngka = findViewById(R.id.angKa);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka++;
                if (txtAngka != null){
                    txtAngka.setText(Integer.toString(angka));
                }
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                angka = 0;
                txtAngka.setText(Integer.toString(angka));
                Toast toast = Toast.makeText(getApplicationContext(),"BALEK",Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }
}
