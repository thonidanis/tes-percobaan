package com.example.tes1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText usernameEditText = findViewById(R.id.et_username);
        final EditText passwordEditText = findViewById(R.id.et_password);
        Button loginButton = findViewById(R.id.btn_login);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                if("Thoni".equals(username) &&
                    "206".equals(password)){
                    Intent intent = new Intent(LoginActivity.this,NavigationActivity.class);
                    startActivity(intent);
                    finish();
                    }else{
                    Toast.makeText(LoginActivity.this, "GAK ISO",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
