package com.example.tes1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CalculatorActivity extends AppCompatActivity {
    Button btnKurang, btnKali, btnBagi, btnTambah;
    EditText edtAngkaPertama, edtAngkaKedua;
    TextView txtHasil;

    @Override
    public void onBackPressed() {
        Intent i = new Intent(CalculatorActivity.this,NavigationActivity.class);
        startActivity(i);
        finish();
    }

    double angkaPertama, angkaKedua, hasil;
    String sAngkaPertama, sAngkaKedua;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        btnTambah = findViewById(R.id.btn_tambah);
        btnKurang = findViewById(R.id.btn_kurang);
        btnKali = findViewById(R.id.btn_kali);
        btnBagi = findViewById(R.id.btn_bagi);
        edtAngkaPertama = findViewById(R.id.edit_text_angka_pertama);
        edtAngkaKedua = findViewById(R.id.edit_text_angka_kedua);
        txtHasil = findViewById(R.id.text_view_hasil);

        btnKurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sAngkaPertama = edtAngkaPertama.getText().toString();
                sAngkaKedua = edtAngkaKedua.getText().toString();
                angkaPertama = Double.parseDouble(sAngkaPertama);
                angkaKedua = Double.parseDouble(sAngkaKedua);
                hasil = angkaPertama - angkaKedua;
                txtHasil.setText(" " + hasil);

            }
        });
        btnKali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sAngkaPertama = edtAngkaPertama.getText().toString();
                sAngkaKedua = edtAngkaKedua.getText().toString();
                angkaPertama = Double.parseDouble(sAngkaPertama);
                angkaKedua = Double.parseDouble(sAngkaKedua);
                hasil = angkaPertama*angkaKedua;
                txtHasil.setText(" "+hasil);
            }


        });
        btnBagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sAngkaPertama = edtAngkaPertama.getText().toString();
                sAngkaKedua = edtAngkaKedua.getText().toString();
                angkaPertama = Double.parseDouble(sAngkaPertama);
                angkaKedua = Double.parseDouble(sAngkaKedua);
                hasil = angkaPertama/angkaKedua;
                txtHasil.setText(" "+hasil);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sAngkaPertama = edtAngkaPertama.getText().toString();
                sAngkaKedua = edtAngkaKedua.getText().toString();
                angkaPertama = Double.parseDouble(sAngkaPertama);
                angkaKedua = Double.parseDouble(sAngkaKedua);
                hasil = angkaPertama+angkaKedua;
                txtHasil.setText(" "+hasil);
            }
        });
    }
}